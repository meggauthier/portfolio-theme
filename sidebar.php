<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
	<div class="sidebar-module sidebar-module-inset">
		<h4>About</h4>
		<p>Description goes here. It's currently hard coded into the theme sidebar.php file.</p>
	</div>
	<div class="sidebar-module">
		<h4>Archives</h4>
		<ol class="list-unstyled">
			<?php wp_get_archives('type=monthly'); ?>
		</ol>
	</div>
	<div class="sidebar-module">
		<h4>Social Media</h4>
		<ol class="list-unstyled">
			<li><a href="https://github.com/blackcj">GitHub</a></li>
			<li><a href="https://twitter.com/chrisjblack">Twitter</a></li>
			<li><a href="https://bitbucket.org/blackcj2">BitBucket</a></li>
		</ol>
	</div>
</div><!-- /.blog-sidebar -->